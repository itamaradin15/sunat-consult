<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Peru\Sunat\Ruc;
use Peru\Http\ContextClient;

class HomeController extends Controller
{
    public function index($ruc){     
        $cs = new Ruc();
        $cs->setClient(new ContextClient());
        
        $company = $cs->get($ruc);
        if ($company === false) {
            return  response()->json($cs->getError());
            exit();
        }
        return response()->json($company);
    }
}
